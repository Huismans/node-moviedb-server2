const assert = require('assert')
const database = require('../src/datalayer/mssql.dao')

describe('MovieDatabase', () => {
  // Testcase
  it('should accept a movie', done => {
    // wat verwachten we dat waar is?
    const movie = {
      title: 'finding nemo',
      description: 'beschrijving',
      year: 2004,
      actors: []
    }

    //
    // Hier moet natuurlijk een insert statement staan, maar dat
    // betekent data bij elke test een nieuwe Movie in de database
    // wordt toegevoegd. Eigenlijk hebben we een aparte test-
    // database nodig hiervoor.
    //
    const query = `SELECT * FROM Movie`
    database.executeQuery(query, (err, result) => {
      if (err) {
        done(err.message)
      }
      if (result) {
        // assert(database.movies[id].title === "finding nemo");
        done()
      }
    })
  })
})
