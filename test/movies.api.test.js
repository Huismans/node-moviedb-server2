const chai = require('chai')
const chaiHttp = require('chai-http')
const jwt = require('jsonwebtoken')

// LET OP: voeg onder aan app.js toe: module.exports = app
const server = require('../app')

chai.should()
chai.use(chaiHttp)

const endpointToTest = '/api/movies'
const authorizationHeader = 'Authorization'
let token

//
// Deze functie wordt voorafgaand aan alle tests 1 x uitgevoerd.
//
before(() => {
  console.log('- before: get valid token')
  // We hebben een token nodig om een ingelogde gebruiker te simuleren.
  // Hier kiezen we ervoor om een token voor UserId 1 te gebruiken.
  const payload = {
    UserId: 1
  }
  jwt.sign({ data: payload }, 'secretkey', { expiresIn: 2 * 60 }, (err, result) => {
    if (result) {
      token = result
    }
  })
})

beforeEach(() => {
  console.log('- beforeEach')
})

describe('Movies API POST', () => {
  it('should return a valid id when posting a valid object', done => {
    chai
      .request(server)
      .post(endpointToTest)
      .set(authorizationHeader, 'Bearer ' + token)
      .send({
        title: 'finding nemo',
        description: 'beschrijving',
        year: 2004,
        actors: []
      })
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.a('object')

        const result = res.body.result
        result.should.be.an('array').that.has.length(1)
        const movie = result[0]
        movie.should.have.property('MovieId')
        movie.should.have.property('Title').that.is.a('string')
        movie.should.have.property('Description').equals('beschrijving')
        movie.should.have.property('Year').equals(2004)
        movie.should.not.have.property('password')
        done()
      })
  })

  it('should throw an error when the database is full', done => {
    done()
  })
})

describe('Movie API GET', () => {
  it('should return an array of Movies', done => {
    // Write your test here
    done()
  })
})

describe('Movie API PUT', () => {
  it('should return the updated Movie when providing valid input', done => {
    // Write your test here
    done()
  })
})

describe('Movie API DELETE', () => {
  it('should return http 200 when deleting a Movie with existing id', done => {
    // Write your test here
    done()
  })
})
