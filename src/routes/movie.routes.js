const express = require('express')
const router = express.Router()
const MovieController = require('../controllers/movie.controller')
const AuthController = require('../controllers/authentication.controller')

// Lijst van movies
router.post('/', AuthController.validateToken, MovieController.createMovie)
router.get('/', MovieController.getAllMovies)
router.get('/:movieId', MovieController.getMovieById)
router.delete('/:movieId', AuthController.validateToken, MovieController.deleteMovieById)

router.post('/:movieId/actors/:actorId', AuthController.validateToken, MovieController.linkActorToMovie)

module.exports = router
