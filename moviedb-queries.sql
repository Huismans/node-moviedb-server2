USE master
GO

USE [MovieAppProgrammeren4]
GO

-- DELETE FROM [Actor]
-- GO
-- -- Restart autoincrement at 0
-- DBCC CHECKIDENT ('Actor', RESEED, 0)
-- GO

-- INSERT INTO [dbo].[Actor]
-- 	(FirstName, LastName)
-- VALUES
-- 	('Nemo', 'Fish'),
-- 	('Dori', 'Fish'),
-- 	('Bruce', 'The Shark');
-- GO

SELECT *
FROM [Actor]
GO

SELECT *
FROM [Movie_Actor]
GO
